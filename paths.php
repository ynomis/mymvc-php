<?php

define('DS', DIRECTORY_SEPARATOR);

define('ROOT',             realpath(__DIR__).DS); // Module's root

define('CORE',             ROOT.'core'.DS);

define('APP_ROOT',         ROOT.'application'.DS);
define('APP_CONTROLLERS',  APP_ROOT.'controllers'.DS);
define('APP_MODELS',       APP_ROOT.'models'.DS);
define('APP_VIEWS',        APP_ROOT.'views'.DS);
define('APP_CONFIGS',      APP_ROOT.'configs'.DS);
define('APP_LIBRARIES',    APP_ROOT.'libraries'.DS);

define('STORAGE',          ROOT.'storage'.DS);
define('STORAGE_CACHE',    STORAGE.'cache'.DS);
define('STORAGE_LOGS',     STORAGE.'logs'.DS);
define('STORAGE_SESSIONS', STORAGE.'sessions'.DS);

define('EXT',              '.php');
define('EXT_TPL',          '.tpl');
define('EXT_CACHE',        '.cache');
define('EXT_LOG',          '.log');
define('EXT_SESSION',      '.session');

define('WEB_ROOT',         '/');
// In case it's in subdirectory, no trailing slash
// define('WEB_ROOT', '/mvc/public'); // Assume entry point is under public folder

chdir(ROOT);
