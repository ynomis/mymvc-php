<?php


namespace MicroMVC;

/**
 * This model class is different to those that handling database access. This is interface
 * between Request and View. It may use db access model class, e.g. AR classes, for database
 * operations. It may not use them at all.
 *
 * @author Simon Yang <syang@salestreamsoft.com>
 *
 */
abstract class Model {
}
