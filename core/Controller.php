<?php

namespace MicroMVC;

abstract class Controller {

    protected $config;

    protected $request;

    protected $response;

    protected $view;

    protected $model;

    /**
     * @var $logger \Monolog\Logger
     */
    protected $logger;

    public function __construct($controller, $action, $args) {
        $this->config = Config::getInstance();

        $this->logger = $this->config->get('logger');

        $slim_env = \Slim\Environment::getInstance();
        $this->request  = new \Slim\Http\Request($slim_env);
        $this->response = new \Slim\Http\Response();

        $format = (is_array($args)) ? end($args) : null;

        $this->view = new View($this->response, $format);
        $this->view->setLayout($this->config->get('defaultLayout'));
        $this->view->setContents($controller . '/' . $action);

        $model_name = $controller . 'Model';
        if (class_exists($model_name)) {
            $this->model = new $model_name();
        }
    }

    public function __destruct() {
        $this->view->render();
    }

    /**
     * Redirect to another MVC resources, in case of conditional situations.
     *
     * @param string $controller
     * @param string $action
     * @param array $args
     */
    public function redirect($controller, $action = "index", $args = array()) {
        $location = static::getUrl($controller, $action, $args);
        $this->response->redirect($location, 303);
    }

    /**
     * Compose the URL path for resources from controller and action name.
     *
     * @param string $controller
     * @param string $action
     * @param array $args
     * @return string
     */
    public static function getUrl($controller, $action = "index", $args = array()) {
    	return WEB_ROOT . "/" . $controller . "/" . $action . "/" . implode("/", $args);
    }

}
