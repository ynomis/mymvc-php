<?php

namespace MicroMVC;

class Config {

    private static $instance;

    private $values = array();

    public function __construct() {
        $this->load('general');
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            $className = __CLASS__;
            self::$instance = new $className();
        }
        
        return self::$instance;
    }

    public function load($name) {
        $path = APP_CONFIGS . $name . '.conf';
        
        if (file_exists($path)) {
            $this->values = parse_ini_file($path);
        }
        
        return $this;
    }

    public function get($value) {
        return (isset($this->values[$value])) ? $this->values[$value] : null;
    }

    public function getAll() {
        return $this->values;
    }

    public function set($key, $value = null) {
        if (!$key)
            return false;
        
        $this->values[$key] = $value;
        
        return $this;
    }
}
