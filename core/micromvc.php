<?php


namespace MicroMVC;

/**
 * Load core settings
 */
include_once CORE . 'core' . EXT;

/**
 * Application bootstrap
 */
include_once APP_ROOT . 'bootstrap' . EXT;
