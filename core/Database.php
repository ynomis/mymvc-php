<?php

namespace MicroMVC;

/**
 * The Simplest PDO interface
 * 
 * @author Simon Yang <simonxy@gmail.com>
 *
 */
class Database {

    private static $conn = array();

    private $config;

    /**
     * private constructor to enforce singleton
     */
    private function __construct() {
        $this->config = Config::getInstance();
    }

    public static function get_connection($server_name = null) {
        $server_name = ($server_name) ?: $_SERVER['SERVER_NAME'];
        $conf = new Config($server_name);

        $db_name    = $this->config->get('dbName');
        $driver     = $this->config->get('dbDriver');
        $host       = $this->config->get('dbHost');
        $port       = $this->config->get('dbPort');
        $dsn = "$driver:dbname=$db_name;host=$host;port=$port";

        if (isset(self::$conn[$dsn])) {
            return self::$conn[$dsn];
        }

        $user       = $this->config->get('dbUser');
        $password   = $this->config->get('dbPassword');
        $options    = array();
        $attributes = array();

        self::$conn[$dsn] = new PDO($dsn, $user, $password, $options);

        $attributes['ATTR_ERRMODE'] = 'ERRMODE_EXCEPTION';

        foreach ( $attributes as $k => $v ) {
            self::$conn[$dsn]->setAttribute(constant("PDO::{$k}"), constant("PDO::{$v}"));
        }

        return self::$conn[$dsn];
    }

    public static function __callStatic($name, $args) {
        $callback = array(self::get_connection(), $name);
        return call_user_func_array($callback, $args);
    }

}

// $stmt = Database::get_connection()->prepare ( "SELECT * FROM `users` LIMIT 1;" ) ;
// $stmt = Database :: prepare ( "SELECT * FROM `users`;" ) ;
// $stmt -> execute ( ) ;
// var_dump ( $stmt -> fetchAll ( ) ) ;
// $stmt -> closeCursor ( ) ;