<?php


namespace MicroMVC;

class Autoloader {

    /**
     * Class directories
     *
     * @var array
     */
    protected static $directories = array ();

    /**
     * Class Aliases
     *
     * @var array
     */
    protected static $aliases = array ();

    /**
     * Class mappings
     *
     * @var array
     */
    protected static $mappings = array ();

    /**
     * The mappings for namespaces to directories
     *
     * @var array
     */
    public static $namespaces = array ();

    /**
     * Has this class been registered as the autoloader?
     *
     * @var string
     */
    private static $_initialized = false;

    /**
     * Register this class to be used for autoloading
     */
    public static function register() {
        // Not initialized yet
        if (! static::$_initialized) {
            // Check if already set up as an autoloader
            $in_spl_autoload = false;
            
            if (spl_autoload_functions ()) {
                foreach ( spl_autoload_functions () as $loader ) {
                    if ($loader [0] == __CLASS__) {
                        $in_spl_autoload = true;
                        
                        continue;
                    }
                }
            }
            
            // It's not set up as an autoloader. Set it up
            if (! $in_spl_autoload) {
                spl_autoload_register ( array ( __CLASS__, 'load' ) );
                
                static::$_initialized = true;
            }
        }
    }

    /**
     * Unregister this class to be used for autoloading
     */
    public static function unregister() {
        // Initialized already
        if (static::$_initialized) {
            spl_autoload_unregister ( array ( __CLASS__, 'load' ) );
            
            static::$_initialized = false;
        }
    }

    /**
     * Load the class file corresponding to a given class
     *
     * @param string $class            
     * @return mixed
     */
    public static function load($class) {
        // This should never happen but in case this class hasn't been registered yet, register it real quick
        if (! static::$_initialized) {
            static::register ();
        }
        
        // Check to see if the class has been aliased
        if (isset ( static::$aliases [$class] )) {
            return class_alias ( static::$aliases [$class], $class );
        }        

        // Load mapped class
        elseif (isset ( static::$mappings [$class] )) {
            return require static::$mappings [$class];
        }
        
        // Namespace
        foreach ( static::$namespaces as $namespace => $directory ) {
            if (preg_match ( '/^' . preg_quote ( $namespace, '/' ) . '/i', $class )) {
                return static::_load ( substr ( $class, strlen ( $namespace ) ), $directory );
            }
        }
        
        // Regular loading of class
        return static::_load ( $class );
    }

    /**
     * Register a class alias
     *
     * @param string $alias            
     * @param string $class            
     */
    public static function alias($alias = null, $class = null) {
        if (! is_array ( $alias )) {
            $alias = array ( $alias => $class );
        }
        
        static::$aliases = array_merge ( static::$aliases, $alias );
    }

    /**
     * Register an array of class to path mappings
     *
     * @param string|array $mappings            
     * @param string $dir            
     */
    public static function map($mappings = array(), $dir = null) {
        if (! is_array ( $mappings )) {
            $mappings = array ( $mappings => $dir );
        }
        
        static::$mappings = array_merge ( static::$mappings, $mappings );
    }

    /**
     * Register multiple directories to be used when loading classes
     *
     * @param string|array $directories            
     */
    public static function directories($directories = array()) {
        if (! empty ( $directories )) {
            $directories = static::_dirFormat ( $directories );
            
            static::$directories = array_unique ( array_merge ( static::$directories, $directories ) );
        }
    }

    /**
     * Map namespaces to directories
     *
     * @param array $mappings            
     * @param string $append            
     */
    public static function namespaces($mappings = array(), $append = '\\') {
        $mappings = static::_formatMappings ( $mappings, $append );
        
        static::$namespaces = array_merge ( $mappings, static::$namespaces );
    }

    /**
     * Load class
     *
     * @param string $class            
     * @param string $directory            
     * @return mixed
     */
    protected static function _load($class, $directory = null) {
        // Replace double backslash with a forward slash
        $name = str_replace ( array ( '\\', '_' ), '/', $class );
        
        $lower = strtolower ( $name );
        
        $directories = (! empty ( $directory )) ? ( array ) $directory : static::$directories;
        
        // Run through known directories for the class
        foreach ( $directories as $directory ) {
            // All lower case name
            if (file_exists ( $path = $directory . $lower . EXT )) {
                return require $path;
            }            

            // Class name as passed
            elseif (file_exists ( $path = $directory . $name . EXT )) {
                return require $path;
            }
        }
        
        return false;
    }

    /**
     * Format an array of namespace to directory mappings.
     *
     * @param array $mappings            
     * @param string $append            
     * @return array
     */
    protected static function _formatMappings($mappings = array(), $append = '\\') {
        $namespaces = array ();
        
        foreach ( $mappings as $namespace => $directory ) {
            $namespace = trim ( $namespace, $append ) . $append;
            
            unset ( static::$namespaces [$namespace] );
            
            $dir = static::_dirFormat ( $directory );
            
            $namespaces [$namespace] = reset ( $dir );
        }
        
        return $namespaces;
    }

    /**
     * Format directories with the proper trailing slash
     *
     * @param array $directories            
     * @return array
     */
    protected static function _dirFormat($directories = array()) {
        return array_map ( function ($directory) {
            return rtrim ( $directory, '/' ) . '/';
        }, ( array ) $directories );
    }
}
