<?php


namespace MicroMVC;

require_once CORE . 'Autoloader' . EXT;

Autoloader::register ();

Autoloader::namespaces ( array ( 'MicroMVC' => CORE ) );

Autoloader::directories ( array ( CORE ) );
