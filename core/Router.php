<?php

namespace MicroMVC;

class Router {

    private $route;

    private $config;

    private $controller;
    private $controller_orig;

    private $action;

    private $args;

    private $query;

    public function __construct() {
        $this->config = Config::getInstance();
    }

    /**
     * To facilitate the easy of composing url path from controller name, I setup a
     * naming convention for controller name:
     * Controller class name must be the same as its filename, in camel case;
     * Controller name in Web URL path is case-insesitive;
     * Two words controller name needs to break up with underscore, '_'
     * eg.
     *   controller class:  ClientCompanyController
     *   file name:         ClientCompanyController.php
     *   web path:          /client_company/action (or /CLIENT_COMPANY/ACTION)
     *
     * @param string $name
     */
    public function setRequest($request) {
        $segments = explode('/', ltrim(strtok($request, '?'), '/'));
        
        $this->controller_orig
            = (isset($segments[0]) && $segments[0] !== '') ? $segments[0] : $this->config->get('defaultController');
        $this->action
            = (isset($segments[1]) && $segments[1] !== '') ? $segments[1] : $this->config->get('defaultAction');
        $this->args
            = array_slice($segments, 2) ?  : null;

        $_ctrl = explode('_', $this->controller_orig);
        array_walk($_ctrl, function(&$item) {
            $item = ucwords(strtolower($item));
        });

        $this->controller = implode('', $_ctrl);
        return $this;
    }

    public function dispatch() {
        $controller_name = $this->controller . 'Controller';
        $action_name = $this->action . 'Action';
        
        if (class_exists($controller_name) && method_exists($controller_name, $action_name)) {
            $controller = new $controller_name($this->controller, $this->action, $this->args);
            $controller->$action_name($this->args);
        } else {
            header("HTTP/1.0 404 Not Found");
            die("path {$this->controller}/{$this->action} does not exist");
        }
        
        return $this;
    }

}
