<?php


namespace MicroMVC;

class View {

    const DEFAULT_BLOCK = 'content';

    private $config;

    private $response; /* @var \Slim\Http\Response */

    private $format     = 'html';

    private $layout     = null;

    private $template   = null;

    private $contents   = array ();

    private $data       = array ();

    public function __construct($response, $format = null) {
        $this->config = Config::getInstance ();
        
        $this->response = $response;
        
        if ($format && strlen ( $format )) {
            $this->format = $format;
        }
    }

    public function setFormat($format) {
        $this->format = $format;
    }

    public function setLayout($name = null) {
        if (! $name)
            return false;
        
        $layout = APP_VIEWS . 'layouts/' . strtolower ( $name ) . EXT_TPL;
        
        if (file_exists ( $layout )) {
            $this->layout = $layout;
        }
        
        return $this;
    }

    /**
     * if input is string, it must be a valid path in APP_VIEWS folder w/o extensions EXT_TPL
     * if input is array, it should follow following convention:
     * array(
     *      'path1', // string representation
     *      array('path2', $data1), // array representation
     *      // ...
     * )
     * while templates represented by string here will be rendered with class data, but array-ed
     * templates willbe rendered with its own data provided.
     *
     * other than above, view will not be rendered correctly.
     *
     * @param mixed $templateIndex
     * @return boolean|\MicroMVC\View
     */
    public function setContents($templateIndex) {
        if (!$templateIndex)
            return false;

        if (is_array($templateIndex)) {
            $this->contents = $templateIndex;
        } elseif (is_string($templateIndex)) {
            $this->contents = array($templateIndex);
        } else {
        	error_log('Un-recognized path for view templates, ' . __CLASS__ . '->' . __FUNCTION__ . '@' . __LINE__);
        }

        return $this;
    }

    /**
     * @see append()
     * this will add view block to the top of content array
     *
     * @param mixed $partial
     * @return boolean|\MicroMVC\View
     */
    public function prepend($partial) {
        if (!$partial) return false;

    	array_unshift($this->contents, $partial);

    	return $this;
    }

    /**
     * Append other views into current view. Same conventions as setContents():
     * string represents path in APP_VIEWS folder w/o extensions EXT_TPL
     * array should be in following format:
     * array('path', $data);
     * In case you just don't want to share the native scope of data, pass empty
     * array as second paramter.
     *
     * @param string $key, key to conent index in layout
     * @param string $partial, uri index in APP_VIEW folder w/o extension
     * @param array $data, data to be binded to the partial view
     * @return boolean|\MicroMVC\View
     */
    public function append($partial) {
        if (!$partial) return false;

        $this->contents[] = $partial;

        return $this;
    }

    public function assign($key, $value = null) {
        // if $key is an array, execute recursion
        if (is_array ( $key )) {
            foreach ( $key as $item => $value ) {
                $this->assign ( $item, $value );
            }
            
            return;
        }
        
        $this->data [$key] = $value;
        
        return $this;
    }

    public function render() {
        if (!$this->response->isRedirection()) {
            switch ($this->format) {
                case 'json' :
                    $this->_renderJson ();
                    break;
                default :
                    $this->_renderHtml ();
            }
        }

        list ( $status, $header, $body ) = $this->response->finalize ();
        
        // Send headers
        if (headers_sent () === false) {
            // Send status
            if (strpos ( PHP_SAPI, 'cgi' ) === 0) {
                header ( sprintf ( 'Status: %s', \Slim\Http\Response::getMessageForCode ( $status ) ) );
            } else {
                header ( sprintf ( '%s %s', $_SERVER['SERVER_PROTOCOL'], \Slim\Http\Response::getMessageForCode ( $status ) ) );
            }
            
            // Send headers
            foreach ( $header as $name => $value ) {
                $hValues = explode ( "\n", $value );
                foreach ( $hValues as $hVal ) {
                    header ( "$name: $hVal", false );
                }
            }
        }
        
        // Send body
        echo $body;
    }

    protected function _renderHtml() {
        $_data = $this->data;
        $content = '';

        array_walk( $this->contents, function (&$_content) use(&$_data) {
            $_ob = function ($path, &$data) {
                $templateUri = APP_VIEWS . strtolower ( $path ) . EXT_TPL;
        
                if (file_exists($templateUri)) {
                    extract($data);
                    ob_start();
                    include ($templateUri);
                    return ob_get_clean();
                } else {
                	error_log('MVC View template does not exist, ' . $templateUri);
                }
            };

            if (is_array($_content)) { // template with its own data
            	$_content = $_ob($_content[0], $_content[1]);
            } else {
                $_content = $_ob($_content, $_data);
            }
        });

        $_name = self::DEFAULT_BLOCK;
        $$_name = implode("\n", $this->contents);
        ob_start ();
        include ($this->layout);
        $this->response->body ( ob_get_clean () );
    }

    protected function _renderJson() {
        $this->response ['Content-Type'] = 'application/json';
        $this->response->body ( json_encode ( $this->data ) );
    }

}
