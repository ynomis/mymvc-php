<?php

class IndexController extends MicroMVC\Controller {

	public function indexAction() {
		$this->view->assign('myvar', 'Hello World!');
		
		$this->logger->addError('This is error from Index controller Index action.');
	}

	public function infoAction($id = null) {
		$array = array('foo' => 'bar', 'baz' => 'doo', 'parm' => $id);
		$this->view->assign('myvar', $array);
		$this->view->assign('param', $id);
		$this->view->assign('get_post', $this->request->get());

		// throw new Exception('The first exception test');
		// trigger_error("Cannot divide by zero", E_USER_ERROR);
		// $this->redirect('user'); // example of redirect within controller
	}
}
