<?php

class UserController extends MicroMVC\Controller {

	public function indexAction() {
		$this->view->setLayout('blank');
		$this->view->assign('myvar', 'Hello World!');

		$model = new ClientCompanyModel();
		$this->view->prepend( array('index/info', $model->prepareClientForm()) );
		// $this->view->assign('user', $this->config->get('loggedin_user'));

	}

}
