<?php

class WarningException              extends ErrorException {}
class ParseException                extends ErrorException {}
class NoticeException               extends ErrorException {}
class CoreErrorException            extends ErrorException {}
class CoreWarningException          extends ErrorException {}
class CompileErrorException         extends ErrorException {}
class CompileWarningException       extends ErrorException {}
class UserErrorException            extends ErrorException {}
class UserWarningException          extends ErrorException {}
class UserNoticeException           extends ErrorException {}
class StrictException               extends ErrorException {}
class RecoverableErrorException     extends ErrorException {}
class DeprecatedException           extends ErrorException {}
class UserDeprecatedException       extends ErrorException {}

/**
 * Error handler, throw exceptions based on E_* error types
 */
function log_error($err_severity, $err_msg, $err_file, $err_line, array $err_context) {
    // error was suppressed with the @-operator
    if (!(error_reporting() & $err_severity)) { return false; }
    switch($err_severity)
    {
    	case E_ERROR:               throw new ErrorException            ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_WARNING:             throw new WarningException          ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_PARSE:               throw new ParseException            ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_NOTICE:              throw new NoticeException           ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_CORE_ERROR:          throw new CoreErrorException        ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_CORE_WARNING:        throw new CoreWarningException      ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_COMPILE_ERROR:       throw new CompileErrorException     ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_COMPILE_WARNING:     throw new CoreWarningException      ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_USER_ERROR:          throw new UserErrorException        ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_USER_WARNING:        throw new UserWarningException      ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_USER_NOTICE:         throw new UserNoticeException       ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_STRICT:              throw new StrictException           ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_RECOVERABLE_ERROR:   throw new RecoverableErrorException ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_DEPRECATED:          throw new DeprecatedException       ($err_msg, 0, $err_severity, $err_file, $err_line);
    	case E_USER_DEPRECATED:     throw new UserDeprecatedException   ($err_msg, 0, $err_severity, $err_file, $err_line);
    }
};

/**
 * Uncaught exception handler.
 * For now, we throw everything to default error_log. Once we have a better Logger,
 * we can revisit this.
 */
function log_exception(Exception $e) {
    $message = "Type: " . get_class($e) . "; Message: {$e->getMessage()}; File: {$e->getFile()}; Line: {$e->getLine()};";
    error_log($message);
    error_log(get_better_trace_as_string($e));
    // in case we need to redirect to a dedicated error page.
    // header( "Location: new_url" );
    
    // exit();
}

/**
 * This one won't trancate as original version of getTaceAsString()
 *
 * @param \Exception $e
 * @return string
 */
function get_better_trace_as_string(Exception $e) {
    $rtn = "";
    $count = 0;
    foreach ( $e->getTrace() as $frame ) {
        $args = "";
        if (isset($frame['args'])) {
            $args = array();
            foreach ( $frame['args'] as $arg ) {
                if (is_string($arg)) {
                    $args[] = "'" . $arg . "'";
                } elseif (is_array($arg)) {
                    $args[] = "Array";
                } elseif (is_null($arg)) {
                    $args[] = 'NULL';
                } elseif (is_bool($arg)) {
                    $args[] = ($arg) ? "true" : "false";
                } elseif (is_object($arg)) {
                    $args[] = get_class($arg);
                } elseif (is_resource($arg)) {
                    $args[] = get_resource_type($arg);
                } else {
                    $args[] = $arg;
                }
            }
            $args = join(", ", $args);
        }
        $rtn .= sprintf("#%s %s(%s): %s(%s)\n", $count, $frame['file'], $frame['line'], $frame['function'], $args);
        $count++;
    }
    return $rtn;
}

/**
 * Checks for a fatal error, work around for set_error_handler not working on fatal errors.
 */
function check_for_fatal() {
    $error = error_get_last();
    if ($error["type"] == E_ERROR)
        log_error($error["type"], $error["message"], $error["file"], $error["line"], $error);
}

register_shutdown_function("check_for_fatal");
set_error_handler("log_error");
set_exception_handler("log_exception");
ini_set("display_errors", "off");
