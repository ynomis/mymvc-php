<?php

namespace MicroMVC;

define('DEV_ENV', true);

Autoloader::directories(array(
	APP_CONTROLLERS,
	APP_MODELS
));

require_once APP_ROOT . 'error_handlers.php';

// Load external models
// require_once(EXT_ROOT . '/models/autoload.php');

require_once ROOT . 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;

$conf = Config::getInstance();

$logger = new Logger('mymvc');
$conf->set('logger', $logger);

// Switch of DebugBar
if (DEV_ENV) {
    if (isset($_GET['debugbar']) && 'start' == $_GET['debugbar']) {
    	$_SESSION['_debugbar_'] = true;
    }

    if (isset($_GET['debugbar']) && 'stop' == $_GET['debugbar']) {
    	if (isset($_SESSION['_debugbar_']))
    	    unset($_SESSION['_debugbar_']);
    }

    if (isset($_SESSION['_debugbar_']) && $_SESSION['_debugbar_']) {
    //     error_reporting(E_ALL | E_STRICT);
        error_reporting( E_ALL ^ E_NOTICE );

        $debugbar = new \DebugBar\StandardDebugBar();

//         $pdoRaw = new \DebugBar\DataCollector\PDO\TraceablePDO(\Database::get_connection());

//         $pdoCollector = new \DebugBar\DataCollector\PDO\PDOCollector();
//         $pdoCollector->addConnection($pdoRaw, 'pdo-raw');

//         $debugbar->addCollector($pdoCollector);

        $debugbar->addCollector(new \DebugBar\Bridge\MonologCollector($logger));
    
        $debugbarRenderer = $debugbar->getJavascriptRenderer()
            ->setBaseUrl('/js/debugbar')
            ->setEnableJqueryNoConflict(false);

        $debugbar["messages"]->addMessage("hello world!"); // @todo, remove me.
        $logger->addInfo('Hello from monolog', array('a' => 1, 'b' => 2)); // @todo, remove me.

        $conf->set('debugbar', $debugbar);
        $conf->set('debugrender', $debugbarRenderer);

        set_exception_handler(function($e) use ($debugbar, $debugbarRenderer) {
            $debugbar['exceptions']->addException($e);
            echo $debugbarRenderer->render();
        });

        set_error_handler(function ($errno, $errstr = '', $errfile = '', $errline = '') {
            if (error_reporting() & $errno) {
                throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
            }
        
            return true;
        });
    } else {
        $logger->pushHandler(new ErrorLogHandler());
    }
}

$logger->addInfo('Hello from monolog outside of debugbar', array('a' => 1, 'b' => 2)); // @todo, remove me

// TODO add exception handler
$request_uri = substr($_SERVER['REQUEST_URI'], strlen(WEB_ROOT));

$router = new Router();
$router->setRequest($request_uri)
       ->dispatch();
