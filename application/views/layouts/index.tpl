<?php $_cf = \MicroMVC\Config::getInstance(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MicroMVC</title>
    <link rel="stylesheet" href="/inc/styles/styles.php" />
    <link rel="stylesheet" href="/inc/styles/new.css" />
    <link rel="stylesheet" href="/mvc/public/stylesheets/components.css" />
    <link rel="stylesheet" href="/mvc/public/stylesheets/jquery-ui-1.10.4/jquery-ui-1.10.4.custom.css" />
    <script type='text/javascript' src='/mvc/public/js/jquery-2.1.0.js'></script>
    <script type="text/javascript" src="/mvc/public/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="/mvc/public/js/jquery.json.js"></script>
    <?php if ($_cf->get('debugrender')) echo $_cf->get('debugrender')->renderHead() ?>
</head>

<body>
<div>
    <header id="header">
        <div class="container">
            Header
        </div>
    </header>

    <div id="content">
        <div class="container">
          <?php echo $content; ?>
        </div>
    </div>

    <footer id="footer">
        <div class="container">
            Footer
        </div>
    </footer>
</div>

<?php if ($_cf->get('debugrender')) echo $_cf->get('debugrender')->render() ?>

</body>
</html>
